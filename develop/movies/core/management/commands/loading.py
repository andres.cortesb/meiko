from django.core.management.base import BaseCommand
from django.core.management import call_command
from time import time

import csv, json
from core import models

csvfilepath  = 'core\movie_metadata.csv'
jsonfilepath = 'core\movies_data.json'

class Command(BaseCommand):
    
    help = 'Modelado y Carga de datos inicales'

    def handle(self, *args, **kwargs):

        start_time = time()

        call_command("makemigrations", "core")
        call_command("migrate")

        total = 0
        data  = {}
        
        with open(csvfilepath, newline='', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
        
            rows  = list(reader)
            total = len(rows)

            for row in rows: 

                if row['director_name'] != '' :
                    validatename(row['director_name'], row['director_facebook_likes'], 'Directors', data)

                if row['actor_1_name'] != '' :
                    validatename(row['actor_1_name'], row['actor_3_facebook_likes'], 'Actors', data)
                if row['actor_2_name'] != '' :
                    validatename(row['actor_2_name'], row['actor_1_facebook_likes'], 'Actors', data)
                if row['actor_3_name'] != 0 :
                    validatename(row['actor_3_name'], row['actor_2_facebook_likes'], 'Actors', data)

                if row['genres'] != '' :
                    validate(row['genres'], 'Genres', data)                
                if row['plot_keywords'] != '' :
                    validate(row['plot_keywords'], 'Keyworks', data)
                if row['language'] != '' :
                    validate(row['language'], 'Language', data)
                if row['country'] != '' :
                    validate(row['country'], 'Country', data)
                if row['content_rating'] !=  '' :
                    validate(row['content_rating'], 'Rating', data)
                
                CreateMovi(row, data)

            with open(jsonfilepath, 'w') as jsonfile:
                jsonfile.write(json.dumps(data['data'], indent=2))
        
        call_command("loaddata", jsonfilepath, verbosity=0)
        
        elapsed_time = time() - start_time
        
        call_command("createsuperuser")

        self.stdout.write("Tiempo de Ejecución: %s Segundos para %s Registros " % (elapsed_time, total))

def validatename(name, face_like, type, arrData = []) :

    try:
        arrData['data']
    except KeyError:
        arrData['data'] = []
    
    try:
        arrData[type]
    except KeyError:
        arrData[type] = []

    if name in arrData[type]:

        return False

    else:
        
        arrData[type].append(name)
        
        key = len(arrData[type])
        
        name_info = {}

        if 'Directors' in type:
            name_info['model']  = "core.Directors"

        if 'Actors' in type:
            name_info['model']  = "core.Actors"
            
        name_info['pk']     = key
        name_info['fields'] = {}
        name_info['fields']['name']            = name
        name_info['fields']['facebook_like']   = Validatecero(face_like)
        
        arrData['data'].append(name_info)
        
    return arrData

def validate(name, type, arrData = []) :

    names = name.split('|')
    nodat = 0

    try:
        arrData['data']
    except KeyError:
        arrData['data'] = []
    
    try:
        arrData[type]
    except KeyError:
        arrData[type] = []

    for text in names: 
    
        if text in arrData[type]:
            nodat = 0
        else:
            
            arrData[type].append(text)
            
            key = len(arrData[type])
            
            name_info = {}

            if 'Genres' in type:
                name_info['model']  = "core.Genres"
            if 'Keyworks' in type:
                name_info['model']  = "core.Keyworks"
            if 'Language' in type:
                name_info['model']  = "core.Language"
            if 'Country' in type:
                name_info['model']  = "core.Country"
            if 'Rating' in type:
                name_info['model']  = "core.Rating"
                
            name_info['pk']     = key
            name_info['fields'] = {}
            name_info['fields']['name'] = text
            
            arrData['data'].append(name_info)
            
    return arrData

def CreateMovi(arr, arrData = []) :

    try:
        arrData['data']
    except KeyError:
        arrData['data'] = []

    try:
        arrData['movie']
    except KeyError:
        arrData['movie'] = []
    
    arrData['movie'].append(arr['movie_title'])
    key = len(arrData['movie'])

    movie_info = {}
    movie_info['model']  = "core.Movies"
    movie_info['pk']     = key
    movie_info['fields'] = {}
    movie_info['fields']['color']                   = arr['color']
    movie_info['fields']['movie_title']             = arr['movie_title'].strip()
    movie_info['fields']['num_critic_for_reviews']  = Validatecero(arr['num_critic_for_reviews'])  
    movie_info['fields']['duration']                = Validatecero(arr['duration'])
    movie_info['fields']['gross']                   = Validatecero(arr['gross'])
    movie_info['fields']['num_voted_users']         = Validatecero(arr['num_voted_users'])
    movie_info['fields']['movie_imdb_link']         = arr['movie_imdb_link']
    movie_info['fields']['num_user_for_reviews']    = Validatecero(arr['num_user_for_reviews'])
    movie_info['fields']['budget']                  = Validatecero(arr['budget'])
    movie_info['fields']['title_year']              = Validatecero(arr['title_year'])
    movie_info['fields']['imdb_score']              = ValidateFloat(arr['imdb_score'])
    movie_info['fields']['aspect_ratio']            = ValidateFloat(arr['aspect_ratio'])
    movie_info['fields']['movie_facebook_likes']    = Validatecero(arr['movie_facebook_likes'])
    movie_info['fields']['cast_total_facebook_likes']   = Validatecero(arr['cast_total_facebook_likes'])
    movie_info['fields']['facenumber_in_poster']        = Validatecero(arr['facenumber_in_poster'])

    if arrData['Rating'] and arr['content_rating'] != '' :
        movie_info['fields']['content_rating_id']           = ValidateNull(arrData['Rating'].index(arr['content_rating'])+1)
    if arrData['Country'] and arr['country'] != '':
        movie_info['fields']['country_id']                  = arrData['Country'].index(arr['country'])+1
    if arrData['Directors'] and arr['director_name'] != '' :
        movie_info['fields']['director_name_id']            = arrData['Directors'].index(arr['director_name'])+1
    if arrData['Language'] and arr['language'] != '' :
        movie_info['fields']['lenguages_id']                = arrData['Language'].index(arr['language'])+1

    arrData['data'].append(movie_info)

    if arr['actor_1_name'] != '' :
        movie_actor = {}
        movie_actor['model']  = "core.ActorsbyMovie"
        movie_actor['fields'] = {}
        movie_actor['fields']['actor']  = arrData['Actors'].index(arr['actor_1_name'])+1
        movie_actor['fields']['movie']  = key
    
        arrData['data'].append(movie_actor)

    if ['actor_2_name'] != '' :
        movie_actor = {}
        movie_actor['model']  = "core.ActorsbyMovie"
        movie_actor['fields'] = {}
        movie_actor['fields']['actor']  = arrData['Actors'].index(arr['actor_2_name'])+1
        movie_actor['fields']['movie']  = key
        arrData['data'].append(movie_actor)
    
    if arr['actor_3_name'] != '' :
        movie_actor = {}
        movie_actor['model']  = "core.ActorsbyMovie"
        movie_actor['fields'] = {}
        movie_actor['fields']['actor']  = arrData['Actors'].index(arr['actor_3_name'])+1
        movie_actor['fields']['movie']  = key
        arrData['data'].append(movie_actor)

    if arr['genres'] != '' :
        
        genres = arr['genres'].split('|')

        for text in genres:
            movie_gender = {}
            movie_gender['model']  = "core.GenresbyMovie"
            movie_gender['fields'] = {}
            movie_gender['fields']['movie']  = key 
            movie_gender['fields']['genre']  = arrData['Genres'].index(text)+1
            arrData['data'].append(movie_gender)

    if arr['plot_keywords'] != '' :
        
        keyw = arr['plot_keywords'].split('|')
        for text in keyw:   
            movie_keywork = {}
            movie_keywork['model']  = "core.KeyworbyMovie"
            movie_keywork['fields'] = {}
            movie_keywork['fields']['movie']  = key
            movie_keywork['fields']['keywork'] = arrData['Keyworks'].index(text)+1
            arrData['data'].append(movie_keywork)
        
    return arrData


def Validatecero(dato) :

    if len(dato) is 0:
        dato = 0

    return int(dato)

def ValidateNull(dato) :

    if dato == '' :
        return False

    return int(dato)

def ValidateFloat(dato) :

    if len(dato) is 0:
        dato = 0

    return float(dato)
