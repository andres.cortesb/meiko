from django.db import models

class Directors(models.Model):
  name           = models.CharField(max_length = 128)
  facebook_like  = models.IntegerField()

  class meta:
    db_table = 'directors'

class Actors(models.Model):
  name          = models.CharField(max_length = 128)
  facebook_like =  models.IntegerField()

  class meta:
    db_table = 'actors'

class Genres(models.Model):
  name  = models.CharField(max_length = 128)
  
  class meta:
    db_table = 'genres'

class Keyworks(models.Model):
  name  = models.CharField(max_length = 128)
  
  class meta:
    db_table = 'keyworks'

class Language(models.Model):
  name  = models.CharField(max_length = 128)
  
  class meta:
    db_table = 'language'

class Country(models.Model):
  name  = models.CharField(max_length = 128)
  
  class meta:
    db_table = 'country'

class Rating(models.Model):
  name  = models.CharField(max_length = 128)
  
  class meta:
    db_table = 'rating'

class Movies(models.Model):
  color                     = models.CharField(max_length = 128, null=True)
  movie_title               = models.CharField(max_length = 512, null=True)
  num_critic_for_reviews    = models.IntegerField()
  duration                  = models.IntegerField()
  gross                     = models.IntegerField()
  num_voted_users           = models.IntegerField()
  movie_imdb_link           = models.CharField(max_length = 512, null=True)
  num_user_for_reviews      = models.IntegerField()
  budget                    = models.BigIntegerField()
  title_year                = models.IntegerField()
  imdb_score                = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
  aspect_ratio              = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
  movie_facebook_likes      = models.IntegerField()
  cast_total_facebook_likes = models.IntegerField()
  facenumber_in_poster      = models.IntegerField()
  content_rating            = models.ForeignKey(Rating, on_delete = models.PROTECT, null=True)
  country                   = models.ForeignKey(Country, on_delete = models.PROTECT, null=True)
  director_name             = models.ForeignKey(Directors, on_delete = models.PROTECT, null=True)
  lenguages                 = models.ForeignKey(Language, on_delete = models.PROTECT, null=True)

  def __str__(self):
    return self.movie_title

  class meta:
    db_table = 'movies'
  
class ActorsbyMovie(models.Model):
  actor = models.ForeignKey(Actors, on_delete = models.PROTECT)
  movie = models.ForeignKey(Movies, on_delete = models.PROTECT)
    
  class meta:
    db_table = 'actorsbymovie'

class GenresbyMovie(models.Model):
  genre = models.ForeignKey(Genres, on_delete = models.PROTECT)
  movie = models.ForeignKey(Movies, on_delete = models.PROTECT)
  
  class meta:
    db_table = 'genresbymovie'

class KeyworbyMovie(models.Model):
  keywork = models.ForeignKey(Keyworks, on_delete = models.PROTECT)
  movie   = models.ForeignKey(Movies, on_delete = models.PROTECT)
  
  class meta:
    db_table = 'keyworkbymovie'
