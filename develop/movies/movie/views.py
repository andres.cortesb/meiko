from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse
import json

from .models import MoviesDash, GenresDash, DirectorsDash, ActorsDash

def getMoviesbyActors(request):
   
  movie_actors = ActorsDash.objects.all().select_related('movie').order_by('actor')
  movie_actors = serializers.serialize('json', movie_actors)
  return HttpResponse(json.dumps(movie_genres_by_year), content_type='application/json')

def getMoviesbyDirectors(request):
  
  movie_directors = MoviesDash.objects.all().select_related('director_name')
  movie_directors = serializers.serialize('json', movie_directors)
  return HttpResponse(json.dumps(movie_directors), content_type='application/json')
  
def getMoviesbyGenres(request):  
   
  movie_genres = GenresDash.objects.all().select_related('movie').order_by('genre')
  movie_genres = serializers.serialize('json', movie_genres)
  return HttpResponse(json.dumps(movie_genres), content_type='application/json')
