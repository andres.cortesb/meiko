from django.db import models

## Models Own
from core.models import Movies, Directors, Actors, GenresbyMovie, ActorsbyMovie

class MoviesDash(Movies):
  class Meta:
    proxy = True
    verbose_name = 'Movie'
    verbose_name_plural = 'Movies'

class GenresDash(GenresbyMovie):
  class Meta:
    proxy = True
    verbose_name = 'Genre'
    verbose_name_plural = 'Genres'

class DirectorsDash(Directors):
  class Meta:
    proxy = True
    verbose_name = 'Director'
    verbose_name_plural = 'Directors'

class ActorsDash(ActorsbyMovie):
  class Meta:
    proxy = True
    verbose_name = 'Actor'
    verbose_name_plural = 'Actors'
