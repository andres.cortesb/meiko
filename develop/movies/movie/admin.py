from django.contrib import admin
from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy
from django.db.models import Max, Min, Sum, Count

## Models Own
from .models import MoviesDash, GenresDash, DirectorsDash

class MoviesAdminSite(AdminSite, admin.ModelAdmin):

  site_header     = ugettext_lazy('Maiko´s Movies') 
  site_title      = ugettext_lazy('Maiko´s Movies')
  index_title     = ugettext_lazy('Dashboard')
  index_template  = 'admin/movie/index.html'
  
  def each_context(self, request):
    context = super(MoviesAdminSite, self).each_context(request)

    movie_gross_mayor = MoviesDash.objects.all().order_by('-gross')[:10]
    context['moviegrossmayor'] = movie_gross_mayor
    
    movie_gross_menor = MoviesDash.objects.all().order_by('gross')[:10]
    context['moviegrossmenor'] = movie_gross_menor
    
    movie_budget_mayor = MoviesDash.objects.all().order_by('-budget')[:7]
    context['moviebudgetmayor'] = movie_budget_mayor
    
    movie_budget_menor = MoviesDash.objects.all().order_by('budget')[:7]
    context['moviebudgetmenor'] = movie_budget_menor

    movie_genres_by_year = GenresDash.objects.all().select_related('movie').order_by('genre')[:10]
    context['movie_genres_by_year'] = movie_genres_by_year
   
    director_face_like = DirectorsDash.objects.all().order_by('-facebook_like')[:5]
    context['director_face_like'] = director_face_like

    return context

movie_admin = MoviesAdminSite()

movie_admin.register(MoviesDash)
movie_admin.register(GenresDash)
movie_admin.register(DirectorsDash)