
Relacionar Base de Datos

Ir al archivo develop\movies\movies\settings.py y agregar los parametros de configuración de su base de datos:   

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '',
        'USER': '',
        'PASSWORD': '*',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}

Ejecutar el ambiente vitual 

Abrir la linea de comando ir a la ruta "meiko" yejecutar el siguiente comando;

.\Scripts\activate

En la linea de comando y a la carpeta donde se encuentra el proyecto "maiko\develop\movies\" y Correr el comando de carga de datos ala base de datos:

python manage.py loading

Agregar el usuario administrador parapoder ingresar al administrador

Para iniciar el servidor, se ejcuta el siguiente comando: 

python manage.py runserver

Luego de ejecutar el comando, abrir su navegador e ingresar a la siguiente url:  

http://localhost:8000/admin

